var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
	udThemeUrl: '/themes/ud_atom/release_2013-08/',
	globalAssetUrl: '/assets/',
	globalThemeUrl: '/assets/release_2013-08/',
	localUrl: 'assets/',
	oneColWidth: 1080,
	twoColWidth: 980,
	panelToggleVisible: false, 		// Panel toggling
	startVersion: 's4',  			// Starting version
	cssName: 'ud_tele_enroll.css',	// Proj Specific CSS	
	jsName: 'ud_tele_enroll.js'		// Proj Specific JS
};



// This is collective object data for Unified desktop
	TIAA_ud.releaseObj = {
		// DO NOT EDIT THE BELOW

		// All these are current default codes
		defaults: {
			proCSS: true,
			left: true,
			header: true,  
			pagetitle: true,
			body: true,
			popups: true,
			proJS: true
		},
		// DO NOT EDIT THE ABOVE CODE


		// FEEL FREE TO UPDATE THE BELOW CODE
		s4: {
			tele_enroll: {
				body: true
			}
		},
		s6: {
			tele_enroll: {
				left: true,
				body: true,
				pagetitle: true
			}
		}
		// FEEL FREE TO UPDATE THE ABOVE CODE

	};



// Load the main Script            
document.write('<script src="'+TIAA_ud.udThemeUrl+'js/ud_main.js'+'"></script>');
