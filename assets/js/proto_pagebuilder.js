!function($, window, document){ var VersionIt, versions;

//polyfill for object.create
if (typeof Object.create !== 'function') {Object.create = function(obj) {function F() {}; F.prototype = obj; return new F(); }; };

// Pubsub Pattern
(function(a){var b=a({});a.subs=function(){b.on.apply(b,arguments)},a.unsubs=function(){b.off.apply(b,arguments)},a.pubs=function(){b.trigger.apply(b,arguments)}})(jQuery)

	VersionIt = {
		init: function(){var sf = this, flag, _fileName;
			// Collect required information
			sf.release = store.get('release');
			_fileName = urlData.getURLname();
			sf.fileName = _fileName.substring(_fileName.lastIndexOf("/") + 1, _fileName.length).split('.').shift();

			// Mapping through object and set the flag value if there is any default
			$.map(TIAA_ud.releaseObj, function(val, key){
				if(key !== sf.release){
					flag = (flag === false) ? false : true;
				}else{
					flag = false;
				};
			});

			// Loop the init function again and again to set the startVersion if nothing is set
			if(sf.release === '' || sf.release === undefined || flag === true){
				store.set('release', TIAA_ud.startVersion);
				setTimeout(function(){
					sf.init.call(sf);
				},500);
			}else{
				// Start the loader function if starversion is available
				sf.loader();
			};

		},
		loader: function(){var sf = this;
			// Get the object value from current version
			sf.relOfCurr = TIAA_ud.releaseObj[sf.release];

			// Map through each data and replace the un available sections with default section
			$.map(TIAA_ud.releaseObj['defaults'], function(val, key){
				if(sf.relOfCurr[sf.fileName][key]){
					// Run getpage with file name
					sf.getPage(sf.release, key, sf.fileName+'_'+key);
				}else{
					// Run the getpage with default values
					sf.getPage('defaults', key)
				};
				// Publish doneLoad event
				$.pubs('doneLoad');
			});
		},

		getPage: function(vers, sections, name){ var sf = this,
			// Set the section name
			sect = (name !== undefined) ? sections = name : sections; 

			// Run this only if there is no CSS or JS calls
			if(sect.indexOf('proCSS') < 0 && sect.indexOf('proJS') < 0){

				// jquery ajax post call - sending version name and section name
				$.ajax({
					type: "POST",
					url: "assets/templates/php_load.php",
					data: {vers:vers, sections:sect}
					}).done(function(data) {
						// After required data is loaded

						// Is section contains body ?
						(sect.indexOf('body') >= 0)
							? !function(){ 
								$('#wrapper').eq(0).append(data); 
								$.pubs('bodyLoaded', [data, sections]);
								}()

							// Is section contains header?
							: (sect.indexOf('header') >= 0) 
								? !function(){ 
									$('.headercontent').eq(0).append(data);
									$.pubs('headLoaded', [data, sections]);
									}()

								// Is section contains pagetitle?
								: (sect.indexOf('pagetitle') >= 0) 
									? !function(){ 
										($('.pageTitle').length) 
											? $('.pageTitle').eq(0).append(data)
											: $('div.pageTitleWrapper').eq(0).append(data);
										$.pubs('titleLoaded', [data, sections]);
										}()

									// Is section contains left?
									: (sect.indexOf('left') >= 0) 
										? !function(){ 
											$('#leftColumn').eq(0).append(data);
											$.pubs('leftLoaded', [data, sections]);
											}()

										// Is section contains popups?
										: (sect.indexOf('popups') >= 0) 
											? !function(){ 
												$('body').eq(0).append(data);
												$.pubs('popupsLoaded', [data, sections]);
												}()

											// All other sections will be appended to body
											: !function(){ 
												$('body').eq(0).append(data);
												$.pubs('otherLoaded', [data, sections]);
												}();
						
					});
			};

			// LoadCSS if the section has CSS 
			(sect.indexOf('proCSS') >= 0) && sf.loadCSS(vers, sections);

			// LoadJS if the section has JS 
			(sect.indexOf('proJS') >= 0) && sf.loadJS(vers, sections);
		},

		loadCSS: function(vers, sections){var _cssRef, _cssLoc;
			// build fragments for CSS
			_cssLoc = (vers === 'defaults')? 'assets/css/' : 'assets/templates/versions/'+vers+'/';
			_cssRef = '<link href="'+_cssLoc+TIAA_ud.cssName+'" rel="stylesheet" type="text/css">';
			
			// Append the fragment to head
			$('head').append(_cssRef);
		},
		loadJS: function(vers, sections){var _jsRef, _jsLoc;
			// build fragments for JS
			_jsLoc = (vers === 'defaults')? 'assets/js/' : 'assets/templates/versions/'+vers+'/';
			_jsRef = '<script type="text/javascript" src="'+_jsLoc+TIAA_ud.jsName+'"></script>';
			$.subs('bodyLoaded', function(){

				// Append the fragment to body
				$('body').append(_jsRef);
			});
		}
		
	};
		$(function(){
			versions = Object.create(VersionIt);
			versions.init();
		});

}(jQuery, window, document, undefined);